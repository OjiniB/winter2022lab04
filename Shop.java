import java.util.Scanner;
public class Shop {
	public static void main(String[] args)
  {
	  final int NUMBER_PRODUCTS = 5;
      Scanner inputs = new Scanner(System.in);
	  System.out.println("\n~~~~~ Welcome to PRINTER SHOP! ~~~~~");
	  //creating an array of products
	  Printer[] products = new Printer[NUMBER_PRODUCTS];
	  for (int i = 1; i < products.length ; i++) {
		  //products[i] = new Printer();
		  System.out.println("\nWhat kind of printer would you like to buy?");
		  //products[i].setPrinterType(inputs.next());
		  String typePrinter = inputs.next();
		  System.out.println("How many papers do you need?");
		  int numPapers = inputs.nextInt();
		  //products[i].setPrinterPages(inputs.nextInt());
		  System.out.println("note: the speed can not exceed more than 900, \n Please choose any other speed..");
		  //products[i].setSpeedPrinter(inputs.nextInt());
		  int speed = inputs.nextInt();
		  //constructor
		  products[i] = new Printer (typePrinter , numPapers, speed);
	
	  }
	  
	  //last product --> last index
	  int lastIndex = products.length-1;
      System.out.println("\n---|before set methods(constructor) printing last product|---");
	  System.out.println("the Printer # " + lastIndex + ": specifications :");
	  System.out.println("the index # " + lastIndex + ": the printer type is=== " + products[lastIndex].getPrinterType());
	  System.out.println("the index # " + lastIndex + ": the printer paper # are=== " + products[lastIndex].getPapersP());
	  System.out.println("the index # " + lastIndex + ": the printer speed is=== " + products[lastIndex].getFastSpeed());
      products[lastIndex].printingSpeed(lastIndex);

	  //set method for 3 fields
	  System.out.println("the Printer # " + lastIndex + ": specifications : ");
	  System.out.println("What kind of printer would you like to buy?");
	  String typePrinter = inputs.next();
	  products[lastIndex].setPrinterType(typePrinter);
	  System.out.println("How many papers do you need?");
	  int numPapers = inputs.nextInt();
	  products[lastIndex].setPrinterPages(numPapers);
	  System.out.println("note: the speed can not exceed more than 900, \n Please choose any other speed..");
	  int speed = inputs.nextInt();
	  products[lastIndex].setSpeedPrinter(speed);
	  System.out.println("\n---|printing from the set methods last product|---");
	  System.out.println("the Printer # " + lastIndex + ": specifications :");
	  System.out.println("the index # " + lastIndex + ": the printer type is=== " + products[lastIndex].getPrinterType());
	  System.out.println("the index # " + lastIndex + ": the printer paper # are=== " + products[lastIndex].getPapersP());
	  System.out.println("the index # " + lastIndex + ": the printer speed is=== " + products[lastIndex].getFastSpeed());
   
      //calling printSpeed method and printing the last product
	  //products[lastIndex].printingSpeed(); (before)
           products[lastIndex].printingSpeed(lastIndex);
	  System.out.println("\n=============== THANK YOU! ^.^ ================");	  
  }		
}



