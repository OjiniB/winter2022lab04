public class Printer {
    private String printerType;
	private int papersP;
	private int fastSpeed;
	
	//constructor
	public Printer(String type, int numPages , int Speed)
	{
	this.printerType = type;
	this.papersP = numPages;
	this.fastSpeed = Speed;
	}
	
	
  public void printingSpeed(int y)
  {
	  int max = 900;
	  
	  if (this.fastSpeed <= max)
	  {
		   System.out.println("\nPrinter # " + y + ": the speed is : " + this.fastSpeed); 
	  }

	  else
	  {
		 System.out.println("\n*Printer # " + y + ": the speed is : " + this.fastSpeed + "*");  
	  }
	  	
  }
  
  
  
  //get methods for 3 fields
  public String getPrinterType()
  {
	  return this.printerType;
  }
  
  public int getPapersP()
  {
	  return this.papersP;
  }
  
  public int getFastSpeed()
  {
	  return this.fastSpeed;
  }
  
  //set methods for 3 fields
  
  public void setPrinterType(String pType)
  {
	  this.printerType = pType;
  }
  
  public void setPrinterPages(int numPages)
  {
	  this.papersP = numPages;
  }
  
  public void setSpeedPrinter(int speedNum)
  {
	  this.fastSpeed = speedNum;
  }
	
	
}


